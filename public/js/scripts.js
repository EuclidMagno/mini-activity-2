function performOperation(op) {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

    if (op=="add") {
        document.getElementById('result').innerHTML = num1 + num2;
    } else if (op=="subtract") {
        document.getElementById('result').innerHTML = num1 - num2;
    } else if (op=="multiply") { 
        document.getElementById('result').innerHTML = num1 * num2;
    } else if (op=="divide") {
        document.getElementById('result').innerHTML = num1 / num2;
    }
}
